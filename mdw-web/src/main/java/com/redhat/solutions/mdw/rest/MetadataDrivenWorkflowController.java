package com.redhat.solutions.mdw.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/rest/mdw/")
public class MetadataDrivenWorkflowController {

	private static final Logger logger = LoggerFactory.getLogger(MetadataDrivenWorkflowController.class);


	@RequestMapping(value = "/{userId}/{groupId}/instance/create", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody InstanceContext createInstance(@PathVariable("userId") String userId, @PathVariable("groupid") String groupId) {
		logger.info("Creating new process instance context");
		return null;
	}

	@RequestMapping(value = "/{userId}/{groupId}/instance/start", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody InstanceContext startInstance(@PathVariable("userId") String userId, @PathVariable("groupid") String groupId, @RequestBody InstanceContext context) {
		logger.info("Actually created the new process instance " + context.toString());

		
		return null;
	}

}
