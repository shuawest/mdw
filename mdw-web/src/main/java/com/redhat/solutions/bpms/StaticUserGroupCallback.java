package com.redhat.solutions.bpms;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.kie.api.task.UserGroupCallback;
import org.kie.api.task.model.Group;
import org.kie.api.task.model.OrganizationalEntity;
import org.kie.internal.task.api.UserInfo;

/**
 * List callback implementation provides user/group information from a flat file.
 * 
 * TODO: update this to load user/group information from classpath:/staticauth/*.properties files
 */
public class StaticUserGroupCallback implements UserGroupCallback, UserInfo {

	public boolean existsGroup(String groupId) {
		return groupId.equals("PM") || groupId.equals("HR");
	}

	public boolean existsUser(String userId) {
		return userId.equals("jiri") || userId.equals("mary") || userId.equals("Administrator");
	}

	public List<String> getGroupsForUser(String userId, List<String> groupIds,
			List<String> allExistingGroupIds) {
		List<String> groups = new ArrayList<String>();
        if (userId.equals("jiri"))
            groups.add("PM");
        else if (userId.equals("mary"))
            groups.add("HR");
        return groups;
	}

	@Override
	public String getDisplayName(OrganizationalEntity arg0) {
		return null;
	}

	@Override
	public String getEmailForEntity(OrganizationalEntity arg0) {
		return null;
	}

	@Override
	public String getLanguageForEntity(OrganizationalEntity arg0) {
		return null;
	}

	@Override
	public Iterator<OrganizationalEntity> getMembersForGroup(Group arg0) {
		return null;
	}

	@Override
	public boolean hasEmail(Group arg0) {
		return false;
	}

}
