var ctlrs = angular.module("app.controllers", ['ngCookies']);


ctlrs.controller("navCtrl", function($scope, $location, $cookieStore, $filter) {
	$scope.menuClass = function(page) {
		var current = $location.path().substring(1);
		return page === current ? "active" : "";
	};
	
	$scope.model = {
		initialized : true,
		users : refUsers,
		categories : refCategories,
		groups : refProcessMetadata,
		user : $filter('filter')(refUsers, function(i){return i.userid === $cookieStore.get('userid');})[0],
		category : $filter('filter')(refCategories, function(i){return i.categoryid === $cookieStore.get('categoryid');})[0],
		group : $filter('filter')(refProcessMetadata, function(i){return i.groupid === $cookieStore.get('groupid');})[0]
	};
	
	$scope.userChange = function(user) {
		if(user) {
			$cookieStore.put('userid',user.userid);
			$scope.model.categories = $filter('filter')(refCategories, function(i){return user.categories.indexOf(i.categoryid) > -1;});
			$scope.model.groups = $filter('filter')(refProcessMetadata, function(i){return ($scope.model.user.membership.indexOf(i.groupid) > -1) && (($scope.model.category)? i.categoryid == $scope.model.category.categoryid : false);});
		} else {
			$cookieStore.remove('userid');
			$scope.model.user = null;
		}
	};
	
	$scope.categoryChange = function(category) {
		if(category) {
			$cookieStore.put('categoryid',category.categoryid);
			$scope.model.groups = $filter('filter')(refProcessMetadata, function(i){return ($scope.model.user.membership.indexOf(i.groupid) > -1) && (($scope.model.category)? i.categoryid == $scope.model.category.categoryid : false);});
		} 
		else {
			$cookieStore.remove('categoryid');
			$scope.model.category = {};
		}
	};
	
	$scope.groupChange = function(group) {
		if(group) {
			$cookieStore.put('groupid',group.groupid);
		} else {
			$cookieStore.remove('groupid');
			$scope.model.group = null;
		}
	};
});

// donut chart   http://jtblin.github.io/angular-chart.js/#doughnut-chart

ctlrs.controller("inboxCtrl", function($scope, $http, Task) {
		
	$scope.model = {
		initialized : false,
		issues : [],
		tasks : []
	};

	Task.get(function(response) {
		$scope.model.initialized = true;
		$scope.model.tasks = response.list;
	}, function(errmsg) {
		$scope.model.initialized = true;
		$scope.model.issues = [ {
			field : "inbox",
			msg : errmsg.status + " " + errmsg.statusText,
			type : "danger"
		} ];
	});
	 
});

ctlrs.controller("createInstanceCtrl", function($scope, $routeParams, $http, $cookieStore, $filter, RuntimeService) {
	
	$scope.createInstanceKey = function() {
		return $routeParams.categoryid + $routeParams.groupid + (new Date()).valueOf();
	};

	$scope.createEmptyInstance = function() {
		var inst = {
			correlationKey : $scope.createInstanceKey(),
			categoryName : $scope.model.category.displayName,
			processName : $filter('filter')(refProcessMetadata, function(i){return i.groupid == $routeParams.groupid;})[0].processName,
			originatorGroup : $scope.model.groupid,
			originatorUser : $scope.model.userid,
			assignedGroup : $scope.model.groupid,
			assignedUser : $scope.model.userid,
			availableAssignees : [],
			nextAssignedGroup : null,
			domainRoot : "initial",
			contextRoot : $scope.createInstanceKey(),
			iteration : 0
		};
		return inst;
	};

	$scope.model = {
		initialized : true,
		isDirty : true,
		isValid : true,
		issues : [],
		categoryid : $routeParams.categoryid,
		category : $filter('filter')(refCategories, function(i){return i.categoryid == $routeParams.categoryid;})[0],
		groupid : $routeParams.groupid,
		process : $filter('filter')(refProcessMetadata, function(i){return i.groupid == $routeParams.groupid;})[0],
		userid : $cookieStore.get('userid'),
		user :  $filter('filter')(refUsers, function(i){return i.userid == $cookieStore.get('userid');})[0],
		newInstance : $scope.createEmptyInstance(),
		domain : {
			accountNumber : null,
			description : ''			
		},
		response : null
	};
	
	$scope.saveInstance = function() {
		//StartProcessCommand
		//    String processId;
	    //    Map<String, Object> parameters 
		var startProcessCommand = {
			processId : 'com.redhat.solutions.bpmsandbox.process4',
			parameters : {'context': $scope.model.newInstance }
		};
		
		RuntimeService.execute( 
			{ deploymentid: 'com.redhat.solutions:bpmSandbox:1.3' }, 
			startProcessCommand, 
			function(response) {
				if (response != null && response.id > 0) {
					$scope.model.response = response;
				}
			}, function(errmsg) {
				$scope.model.initialized = true;
				$scope.model.issues = [ {
					field : "Instance",
					msg : errmsg.status + " " + errmsg.statusText,
					type : "danger"
				} ];
			});
	};
		 
});

ctlrs.controller("aboutCtrl", function($scope, $http) {

});
