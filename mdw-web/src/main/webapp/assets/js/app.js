var app = angular.module("mdw-web", 
		["ngRoute", 'ngCookies',
		 "app.controllers", "app.services", "app.filters", "app.directives"]
);

app.config(function($routeProvider, $locationProvider, $httpProvider) {
	$locationProvider.hashPrefix('!');
	
	$routeProvider. 
        when("/about", { templateUrl: "assets/partials/about.html", controller: "aboutCtrl" }).
		when("/inbox", { templateUrl: "assets/partials/inbox.html", controller: "inboxCtrl" }).
		when("/create/:categoryid/:groupid", { templateUrl: "assets/partials/create.html", controller: "createInstanceCtrl" }).
	    otherwise( { redirectTo: "/about" });
	
    $httpProvider.defaults.headers.post['Accept'] = 'application/xml';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/xml; charset=utf-8';
    $httpProvider.defaults.headers.post['Access-Control-Max-Age'] = '1728000';
    $httpProvider.defaults.headers.common['Access-Control-Max-Age'] = '1728000';
    $httpProvider.defaults.headers.common['Accept'] = 'application/json';
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
});   
