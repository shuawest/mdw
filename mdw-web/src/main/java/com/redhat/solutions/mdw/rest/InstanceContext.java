package com.redhat.solutions.mdw.rest;

public class InstanceContext implements java.io.Serializable {
	static final long serialVersionUID = 1L;

	private String assignedGroup;
	private String assignedUser;
	private String categoryName;
	private String contextRoot;
	private String correlationKey;
	private String domainRoot;
	private Long iteration;
	private String nextAssignedGroup;
	private String originatorGroup;
	private String originatorUser;
	private String processName;

	public InstanceContext() {	}

	public String getAssignedGroup() {
		return assignedGroup;
	}

	public void setAssignedGroup(String assignedGroup) {
		this.assignedGroup = assignedGroup;
	}

	public String getAssignedUser() {
		return assignedUser;
	}

	public void setAssignedUser(String assignedUser) {
		this.assignedUser = assignedUser;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getContextRoot() {
		return contextRoot;
	}

	public void setContextRoot(String contextRoot) {
		this.contextRoot = contextRoot;
	}

	public String getCorrelationKey() {
		return correlationKey;
	}

	public void setCorrelationKey(String correlationKey) {
		this.correlationKey = correlationKey;
	}

	public String getDomainRoot() {
		return domainRoot;
	}

	public void setDomainRoot(String domainRoot) {
		this.domainRoot = domainRoot;
	}

	public Long getIteration() {
		return iteration;
	}

	public void setIteration(Long iteration) {
		this.iteration = iteration;
	}

	public String getNextAssignedGroup() {
		return nextAssignedGroup;
	}

	public void setNextAssignedGroup(String nextAssignedGroup) {
		this.nextAssignedGroup = nextAssignedGroup;
	}

	public String getOriginatorGroup() {
		return originatorGroup;
	}

	public void setOriginatorGroup(String originatorGroup) {
		this.originatorGroup = originatorGroup;
	}

	public String getOriginatorUser() {
		return originatorUser;
	}

	public void setOriginatorUser(String originatorUser) {
		this.originatorUser = originatorUser;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		InstanceContext that = (InstanceContext) o;
		if (correlationKey != null ? !correlationKey.equals(that.correlationKey) : that.correlationKey != null)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + (correlationKey != null ? correlationKey.hashCode() : 0);
		return result;
	}
}
