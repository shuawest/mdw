
//Stubbed reference data - will be retrieved via service

//bpm server deployments
var processList = [ { deploymentId:'com.redhat.solutions:bpmSandbox:1.3', processId:'com.redhat.solutions.bpmsandbox.process4', processName:'Process4' },
                    { deploymentId:'com.redhat.solutions:bpmSandbox:1.2', processId:'com.redhat.solutions.bpmsandbox.process3', processName:'Process3' },
                 { deploymentId:'com.redhat.solutions:bpmSandbox:1.2', processId:'com.redhat.solutions.bpmsandbox.process2', processName:'Process2' },
					{ deploymentId:'com.redhat.solutions:bpmSandbox:1.2', processId:'com.redhat.solutions.bpmsandbox.process1', processName:'Process1' },
                 { deploymentId:'com.redhat.solutions:mdw-core:1.0', processId:'com.redhat.solutions.mdwcore.GenericMetadataDrivenProcess', processName:'GenericMetadataDrivenProcess' } ];

//users
var refUsers = [ 
	{ userid:'jowest', displayName:'Josh West', membership:[1,2,3,4,5,6,7,8,9,10,12,11,13,14], categories:['CO','BL','CI'] },
	{ userid:'jholmes', displayName:'Justin Holmes', membership:[1,2,3,4,5,6,7,8,9,10,11], categories:['CO'] },
	{ userid:'bmortimo', displayName:'Brian Mortimore', membership:[5,6,7,8,9,10,11], categories:['CO'] },
	{ userid:'mwalz', displayName:'Mark Walz', membership:[6,7,8,9], categories:['CO'] },
	{ userid:'jmarx', displayName:'John Marx', membership:[1,2,12,15], categories:['CO','BL','CI'] } ];

//
var refCategories = [ {categoryid:'CO', displayName:'Client Onboarding'}, 
              {categoryid:'BL', displayName:'Billing'}, 
              {categoryid:'CI', displayName:'Client Inquiry'} ];  

//Process Metadata that drives the generic workflow
var refProcessMetadata = [  
	{ groupid:1, displayName:'Sales Team', processName:'Sales', categoryid:'CO' },
	{ groupid:2, displayName:'Legal Team', processName:'Legal', categoryid:'CO' },
	{ groupid:3, displayName:'Risk', processName:'Risk', categoryid:'CO' },
	{ groupid:4, displayName:'Compliance', processName:'Compliance', categoryid:'CO' },
	{ groupid:5, displayName:'KYC', processName:'KYC', categoryid:'CO' },
	{ groupid:6, displayName:'Tax', processName:'Tax', categoryid:'CO' },
	{ groupid:7, displayName:'GSP Account Set Up', processName:'Account Set Up', categoryid:'CO' },
	{ groupid:8, displayName:'CMS Account Set Up', processName:'Account Set Up', categoryid:'CO' },
	{ groupid:9, displayName:'IMMS Account Set Up', processName:'Account Set Up', categoryid:'CO' },
	{ groupid:10, displayName:'Account Activation', processName:'Activation', categoryid:'CO' },
	{ groupid:11, displayName:'Account Maintenance', processName:'Maintenance', categoryid:'CO' },
	{ groupid:12, displayName:'Tax', processName:'Tax', categoryid:'BL' },
	{ groupid:13, displayName:'Sales', processName:'Sales', categoryid:'BL' },
	{ groupid:14, displayName:'Frontline Inquiry Review', processName:'Inquiry Review', categoryid:'CI' } ];
