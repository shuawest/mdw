var serviceModule = angular.module("app.services", [ "ngResource" ]);

serviceModule.factory("Task", function($resource) {
	return $resource("/business-central/rest/task/query");
});

// https://github.com/droolsjbpm/droolsjbpm-integration/blob/master/kie-remote/kie-remote-rest-api/src/main/java/org/kie/remote/services/rest/api/RuntimeResource.java
serviceModule.factory("RuntimeService", function($resource) {
	return $resource("/business-central/rest/runtime/:deploymentid/execute", { deploymentid: '@deploymentid' }, 
	{ 
		execute: { method:'POST', 'headers':{'Accept':'application/xml','Content-Type':'application/xml; charset=utf-8'}}
	});
});

